"""
kernel_forms.py

Functions for kernel weighting distributions.

Created on Thu Jul  9 13:34:15 2020

Authored by
Alistair Curd
Peckham lab
School of Molecular and Cellular Biology
Faculty of Biological Sciences
University of Leeds

---
Copyright 2020 Alistair Curd

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at
http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the License for the
specific language governing permissions and limitations under the License.
"""

import numpy as np


def gaussian(distance_from_centre, width):
    """Gaussian distribution for kernel weighting.

    Args:
        distance_from_centre (float or numpy array):
            datapoint location - mean of Gaussian distribution.
            datapoint location - KDE evalution location.
        width (float):
            Standard deviation in Gaussian distribution.
            Characteristic width of kernel.

    Returns:
        weighting (float):
            Value of Gaussian function at distance_from_centre.
    """
    weighting = 1. / (width * np.sqrt(2. * np.pi)) \
        * np.exp(-1. * distance_from_centre ** 2 / (2. * width ** 2))
    return weighting
