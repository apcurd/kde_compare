"""define_kernel.py

Library for defining Kernel class objects for use in evaluating kernel
performance in kernel density estimation (KDE).

Alistair Curd
University of Leeds
02 July 2020

---
Copyright 2020 Alistair Curd

Licensed under the Apache License, Version 2.0 (the "License"); you may
not use this file except in compliance with the License. You may obtain
a copy of the License at http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
implied. See the License for the specific language governing
permissions and limitations under the License.
"""

import warnings

import numpy as np

# import kernel_forms as kf


def combine_within_4_widths(data_location, eval_location, width):
    """Decide whether a data point should be included in KDE.

    Only include data points in a KDE if they are within 4 bandwidths
    of the KDE evalution location.

    Args:
        data_location (numpy array, float or int):
            Location of the data point.
        eval_location (float or int):
            Location of the point at which the KDE is being evaluated.
        width (float or int):
            Characteristic width of the kernel use to calculate the KDE.
    Returns:
        bool
    """
    return np.abs(data_location - eval_location) <= 4 * width


class Kernel:
    """Sets up a Kernel object for use in KDE.

    Kernel object has a bandwidth, distribution and
    method of calculating the KDE at an evaluation location.

    Attributes
    ----------
    width (float):
        Characteristic width of the kernel.

    Methods
    -------
    form:
        The parametric form of the kernel.
        Some useful ones are in kernel_forms.py.
    inclusion_method:
        Method for selecting data points to include in
        the KDE at an evalution location.
    """

    def __init__(self, width, form, inclusion_method='standard'):
        """Initialise a Kernel object.

        Parameters
        ----------
        width (float):
            Characteristic width of the kernel.

        form (function):
            Function for the parametric form of the kernel.
            Some useful ones are in kernel_forms.py.

        inclusion_method (str):
            'standard' or function.

            The method for selecting data points to include in
            the KDE at an evalution location. There may be a common
            method between some kernel forms but other may require a
            different method (e.g. cutoff for including data in
            Epanechnikov kernel).

        Returns
        -------
        None.
        """
        self.width = width

        if not callable(form):
            raise TypeError('Form should be a function '
                            '(not a value input, list or string).')
        self.form = form

        # Use either the 'standard' function or another, for the Kernel.form
        if inclusion_method == 'standard':
            self.inclusion_method = combine_within_4_widths
        else:
            if not callable(inclusion_method):
                raise TypeError('Apart from "standard", form should be a '
                                'function (not a value input, '
                                'list or string).')
            self.inclusion_method = inclusion_method


def check_types_extent_length(extent=None, length=None):
    """Check the types of extent and length when building data dictionaries.

    Parameters
    ----------
    extent : float or numpy array of floats (ints allowed), optional
        The maximum value in each dimension for data points to
        include in the KDE. The default is None.
    length : int, optional
        The number of data points to include in the KDE.
        The default is None.

    Returns
    -------
    extent : numpy array
        The input _extent_ converted to a numpy array if given as a
        float or int.
    """
    # Check extent is sensible, if given.
    # If a single value, make it a numpy array for consistency/indexing.
    if extent is not None:
        if not isinstance(extent, (int, float, np.ndarray)):
            raise TypeError('extent must be int, float or numpy array.')
        # Convert to extent single-value numpy array if necessary
        if isinstance(extent, (int, float)):
            extent = np.array([extent])

    # Check length is sensible, if given
    if length is not None:
        if not isinstance(length, int):
            raise TypeError('length must have type int.')

    return extent


def setup_input_data(data_points=None, length=None, extent=None,
                     distribution=None, sampling_method='standard',
                     source_file=None):
    """Set up an input data dictionary.

    Args:
        data_points : numpy array, optional
            The array of data points to be used for KDE. For 1D data,
            array should be 1D. For data with 2D or more, array should
            contain one data point per row.
            Can be modified by _length_ and _extent_.
            The default is None.
        length : int, optional
            The number of data points to include in the KDE. This
            can be used to limit both experimental and simulated
            datasets. The default is None.
        extent : float or numpy array of floats (ints allowed), optional
            The maximum value in each dimension for data points to
            include in the KDE. This can be used to limit both
            experimental and simulated datasets.
            The default is None.
        distribution : function, optional
            The distribution from which simulated data points will
            be/were sampled. The default is None.
        sampling_method : 'standard' or function, optional
            The method for sampling data points from a distribution.
            The default is 'standard', which will refer to a commonly-
            used method.
        source_file : str, optional
            Location of a source file from which to load data
            points. The default is None.

    Returns
    -------
    input_data : dict
        Keys:
            data_points : numpy array
                The returned data points for use in KDE.
                For 1D data the array is 1D. For data with 2D or more,
                the array contains one data point per row.
            extent : numpy array
                The upper limits of allowed values of the data
                points in all dimensions.
            original_length : int
                The number of data points in a dataset, within any
                extent set, before choosing a subset from within it.
            length : int
                The number of data points in the returned data.
            source : str
                Where the data points came from.
                'array' :
                    A numpy array was used directly to create the data.
                'theoretical' :
                    The data was sampled from a theoretical
                    distribution.
                'experimental' :
                    The data was obtained experimentally.
    """
    # Check data_points is sensible, if given
    if data_points is not None:
        if not isinstance(data_points, np.ndarray):
            raise TypeError('data_points must be None or a numpy array.')

    # Check for sensible extent and length
    extent = check_types_extent_length(extent, length)

    # Check that incompatible arguments are not present
    if data_points is not None and (distribution is not None
                                    or sampling_method != 'standard'
                                    or source_file is not None):
        raise RuntimeError('You cannot specify both data_points '
                           '(array input) and distribution '
                           '(theoretical data generation), '
                           'sampling_method (theoretical data '
                           'generation) or source_file (use '
                           'experimental data)')
    if source_file is not None and (distribution is not None
                                    or sampling_method != 'standard'):
        raise RuntimeError('You cannot specify both source_file '
                           '(experimental data) and distribution or '
                           'sampling_method (theoretical data '
                           'generation).')

    # Set up dictionary from array input
    if isinstance(data_points, np.ndarray):
        input_data = setup_input_data_from_array(data_points,
                                                 length=length,
                                                 extent=extent)
        return input_data

    # Set up dictionary from theoretical input
    if distribution is not None:
        input_data = setup_input_data_theoretical(
            distribution,
            sampling_method=sampling_method,
            extent=extent,
            length=length)
        return input_data


def setup_input_data_from_array(data_points, length=None, extent=None):
    """Set up the input_data dictionary from numpy array input.

    Args
    ----
    data_points : numpy array
        The array of data points to be used for KDE. For 1D data,
        array should be 1D. For data with 2D or more, array should
        contain one data point per row.
        Can be modified by _length_ and _extent_.
        The default is None.
    length : int, optional
        The number of data points to include in the KDE.
        The default is None.
    extent : float or 1D numpy array of floats, optional
        The maximum value in each dimension for data points to
        include in the KDE. The default is None.

    Returns
    -------
    input_data : dict
        Keys:
            data_points : numpy array
                The returned data points for use in KDE.
                For 1D data the array is 1D. For data with 2D or more,
                the array contains one data point per row.
            extent : numpy array
                The upper limits of allowed values of the data
                points in all dimensions.
            original_length : int
                The number of data points in a dataset, within any
                extent set, before choosing a subset from within it.
            length : int
                The number of data points in the returned data.
            source : 'array'
                Indicates that a user-specified numpy array was the
                source of the data.

    """
    # Check input data is sensible
    if not isinstance(data_points, np.ndarray):
        raise TypeError('data_points must be a numpy array')
    if data_points.ndim > 2:
        raise ValueError('data_points can only have either one or '
                         'two dimensions. For more than one '
                         'dimension, use one row per data point, '
                         'e.g. [X, Y, Z]')

    # Check extent and length types
    extent = check_types_extent_length(extent, length)

    # Check extent, if given, will work with data_points
    if extent is not None:
        if data_points.ndim == 1:
            if len(extent) != 1:
                raise ValueError('Length of extent, when it is '
                                 'given, must match dimensionality of the '
                                 'data.')
        if data_points.ndim == 2:
            if len(extent) != len(data_points[0]):
                raise ValueError('Length of extent, when it is '
                                 'given, must match dimensionality of the '
                                 'data.')

    # Set up dictionary to populate with data and return.
    input_data = {}
    input_data['source'] = 'array'

    # Limit data_points to any extent given
    # Add extent to data dictionary
    if extent is not None:
        if data_points.ndim == 1:
            data_points = data_points[data_points <= extent]
        else:
            selection = np.all(data_points <= extent, axis=1)
            data_points = data_points[selection]
        input_data['extent'] = extent
    # Or use max(data) as extent
    else:
        extent = np.array([])
        if data_points.ndim == 1:
            extent = np.append(extent, np.max(data_points))
        else:
            transposed_data = data_points.transpose()
            for dim in range(len(data_points[0])):
                extent = np.append(extent, np.max(transposed_data[dim]))
        input_data['extent'] = extent

    # Add length and original length to dictionary and filter data
    # if necessary
    input_data['original_length'] = len(data_points)
    if length is None:
        input_data['length'] = len(data_points)
    elif length > len(data_points):
        warnings.warn('number of data points requested (length) was '
                      'greater than original_length and will be changed '
                      'to be equal to original_length',
                      UserWarning)
        input_data['length'] = len(data_points)
    else:
        # When length variable is less than len(data_points), use length
        # in data dictionary
        input_data['length'] = length
        # Make sure data_points is not sorted to bias removal of data
        data_points = np.random.permutation(data_points)
        # And truncate
        data_points = data_points[:length]

    # Add data_points to dictionary
    input_data['data_points'] = data_points

    return input_data


def setup_input_data_theoretical(distribution,
                                 sampling_method='standard',
                                 extent=np.array([10.]),
                                 length=1000):
    """Set up the input data by sampling from a theoretical distribution.

    Parameters
    ----------
    distribution : function
        The density distribution from which simulated data points will
        be sampled. The default is None.
    sampling_method : 'standard' or function, optional
        The method for sampling data points from a distribution.
        The default is 'standard', which will refer to a commonly-used
        method.
    extent : numpy array (float or int will be converted for
                          consistency/indexing)
        The upper limits of allowed values of the data
        points in all dimensions. The default is [10.]
    length : int
        The number of data points to include in the KDE.
        The default is 1000.

    Returns
    -------
    input_data : dict
        Keys:
            data_points : numpy array
                The returned data points for use in KDE.
                For 1D data the array is 1D. For data with 2D or more,
                the array contains one data point per row.
            distribution: str
                Name for the function from which the data is sampled.
            sampling_method: str
                Name for the method of sampling the data.
            extent : numpy array
                The upper limits of allowed values of the data
                points in all dimensions.
            length : int
                The number of data points in the returned data.
            source : 'theoretical'
                Indicates that a user-specified numpy array was the
                source of the data.
    """
    input_data = {}

    return input_data


def get_experimental_data():
    """Do F."""
    # Include restrict for length and extent.


def sample_from_distribution():
    """Do F."""


def make_synthetic_data():
    """Do F."""
    # Include restrict for length and extent.
