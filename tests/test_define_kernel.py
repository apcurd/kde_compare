"""
test_define_kernel.py

Functions for testing define_kernel.py.

Created on Tue Jul 7 17:14:00 2020

Authored by
Alistair Curd
Peckham lab
School of Molecular and Cellular Biology
Faculty of Biological Sciences
University of Leeds

---
Copyright 2020 Alistair Curd

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at
http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the License for the
specific language governing permissions and limitations under the License.
"""


# import unittest

from pytest import raises
from pytest import warns
import numpy as np
from numpy import testing as nptest

import define_kernel as defk
import kernel_forms as kf


def test_combine_within_4_widths():
    """Test combine_within_4_widths in define_kernel.py.

    Check results.
    """
    # Check single data_location within bandwidth
    assert defk.combine_within_4_widths(5, 5, 2)
    assert defk.combine_within_4_widths(9, 5, 1)
    assert defk.combine_within_4_widths(7, 5, 1)
    assert not defk.combine_within_4_widths(10, 5, 1)
    assert defk.combine_within_4_widths(2, 5, 1)
    assert defk.combine_within_4_widths(1, 5, 1)
    assert not defk.combine_within_4_widths(0, 5, 1)

    assert defk.combine_within_4_widths(5., 5., 2.)
    assert defk.combine_within_4_widths(9., 5., 1.)
    assert defk.combine_within_4_widths(7., 5., 1.)
    assert not defk.combine_within_4_widths(10., 5., 1.)
    assert defk.combine_within_4_widths(2., 5., 1.)
    assert defk.combine_within_4_widths(1., 5., 1.)
    assert not defk.combine_within_4_widths(0., 5., 1.)

    # Check result for array input for data_location
    data_locations = np.array([-5,
                               0, 1, 2, 3, 4, 5, 6,
                               7, 8, 9, 10, 11, 12, 13])
    expected_output = np.array([False,
                                False, False, True, True, True, True, True,
                                True, True, True, True, False, False, False])
    nptest.assert_array_equal(
        defk.combine_within_4_widths(data_locations, 6, 1),
        expected_output
        )


def test_kernel_init():
    """Test initialising the Kernel class object."""
    test_kernel = defk.Kernel(4.,
                              kf.gaussian,
                              inclusion_method='standard')
    assert test_kernel.width == 4.
    expected_form = kf.gaussian
    assert test_kernel.form is expected_form
    assert test_kernel.inclusion_method is \
        defk.combine_within_4_widths

    with raises(TypeError):
        test_kernel = defk.Kernel(4., [0.5, 0.7, 0.5])
    with raises(TypeError):
        test_kernel = defk.Kernel(4., 'gaussian')

    with raises(TypeError):
        test_kernel = defk.Kernel(4., kf.gaussian, [False, True, False])
    with raises(TypeError):
        test_kernel = defk.Kernel(4., kf.gaussian, [0, 1, 0])


def test_setup_input_data():
    """Test initialising the InputData class object."""
    # Test for data_points is None or a numpy array
    with raises(TypeError):
        defk.setup_input_data(data_points=[6, 7])

    # Test for expected results
    testdata_1d = np.arange(100.)
    testextent = 50.
    testlength = 30

    setup_1d = defk.setup_input_data(testdata_1d,
                                     length=testlength,
                                     extent=testextent)
    assert np.max(setup_1d['data_points']) <= testextent
    assert len(setup_1d['data_points']) == testlength
    nptest.assert_equal(setup_1d['extent'], np.array(testextent))
    assert isinstance(setup_1d['extent'], np.ndarray)
    assert setup_1d['length'] == testlength
    assert isinstance(setup_1d['length'], int)
    assert setup_1d['original_length'] == \
        len(testdata_1d[testdata_1d <= testextent])


def test_check_types_extent_length():
    # For type of extent
    with raises(TypeError):
        defk.check_types_extent_length(extent=[3, 4])
    # For integer length of dataset
    with raises(TypeError):
        defk.check_types_extent_length(length=3.2)
    with raises(TypeError):
        defk.check_types_extent_length(length=np.array([54]))


def test_setup_input_data_from_array():
    # For too many dimensions in input array
    testdata = np.ones((2, 4, 5))
    with raises(ValueError, match='X, Y, Z'):
        defk.setup_input_data_from_array(data_points=testdata)

    # Test results
    # No length or extent input
    testinputlength = 100
    testdata_1d = np.arange(float(testinputlength))

    setup_1d = defk.setup_input_data_from_array(testdata_1d)
    nptest.assert_equal(setup_1d['data_points'], testdata_1d)
    nptest.assert_equal(setup_1d['extent'], np.max(testdata_1d))
    assert isinstance(setup_1d['extent'], np.ndarray)
    assert setup_1d['length'] == len(testdata_1d)
    assert isinstance(setup_1d['length'], int)
    assert setup_1d['original_length'] == testinputlength
    assert setup_1d['source'] == 'array'

    # With extent input
    testextent = 50.

    setup_1d = defk.setup_input_data_from_array(testdata_1d, extent=testextent)
    assert np.max(setup_1d['data_points']) <= testextent
    assert len(setup_1d['data_points']) == \
        len(testdata_1d[testdata_1d <= testextent])
    nptest.assert_equal(setup_1d['extent'], np.array(testextent))
    assert isinstance(setup_1d['extent'], np.ndarray)
    assert setup_1d['length'] == len(testdata_1d[testdata_1d <= testextent])
    assert isinstance(setup_1d['length'], int)
    assert setup_1d['original_length'] == \
        len(testdata_1d[testdata_1d <= testextent])

    # With extent and length input
    testlength = 30

    setup_1d = defk.setup_input_data_from_array(testdata_1d,
                                                length=testlength,
                                                extent=testextent)
    assert np.max(setup_1d['data_points']) <= testextent
    assert len(setup_1d['data_points']) == testlength
    nptest.assert_equal(setup_1d['extent'], np.array(testextent))
    assert isinstance(setup_1d['extent'], np.ndarray)
    assert setup_1d['length'] == testlength
    assert isinstance(setup_1d['length'], int)
    assert setup_1d['original_length'] == \
        len(testdata_1d[testdata_1d <= testextent])

    # With length > original length
    testlength = 150
    with warns(UserWarning, match='greater than'):
        setup_1d = defk.setup_input_data_from_array(testdata_1d, testlength)
    assert setup_1d['length'] == len(testdata_1d)
