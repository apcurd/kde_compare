"""
rejection_sampling_test.py

Test the rejection sampling function in theoretical_densities.py.

Authored by
Alistair Curd
Peckham lab
School of Molecular and Cellular Biology
Faculty of Biological Sciences
University of Leeds

---
Copyright 2020 Alistair Curd

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at
http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the License for the
specific language governing permissions and limitations under the License.
"""

import time

import matplotlib.pyplot as plt
import numpy as np

import theoretical_densities as theor
from theoretical_densities import rejection_sample as rejsamp
from theoretical_densities import DensityWithParams


def plot_normalised_density(density, max_search_step):
    """Plot the density function from which we are sampling.

    Args:
        density_function (function):
            The density function.
        density_function_params (list):
            Arguments to the probability density function.
        min_value (float):
            Minimum bound for the simulated data.
        max_value (float):
            Maximum bound for the simulated data.
        max_search_step (float):
            The step size over density function at which to find the
            function maximum.
        normalise (bool):
            Whether the density function needs normalising. Leave as
            False (default) if the function already integrates to 1.
            Set to true if it does not (or you don't know.)

    Returns:
        fig (matplotlib Figure):
            Figure containing the plot
        ax (matplotlib Axes):
            The plot.
    """
    x_values = np.arange(density.limits[0],
                         density.limits[1],
                         max_search_step)
    density_values = density.distribution(x_values,
                                          *density.params) \
        * density.norm_factor

    fig = plt.figure()
    axes = fig.add_subplot(111)
    axes.plot(x_values, density_values)
    fig.show()
    plt.pause(1.)  # Give time to allow the Figure to be displayed.

    # Choose whether to start again
    # with a different function/step-size/bounds.
    choice = input('Do you wish to continue using this function, '
                   'bounds and step size (y/n)? ')
    # Stop if we want to change the function/step-size
    if choice not in ('y', 'Y'):
        print('You chose \'' + choice + '\'. Stopping now.')
        return 'stop'

    return fig, axes


def main():
    """Plot the sample distribution with its density_function."""
    # Inputs to the rejection sampling function.
    wavelength = 10.
    density_with_params = DensityWithParams(theor.sine_plus_one,
                                            [wavelength],
                                            (0., 30.))
    density_with_params.get_norm_factor()
    final_size = 1000
    max_search_step = 0.1
    sampling_max_multiplier = 1.05

    # Plot the density_function (normalised) and inspect.
    plot_result = plot_normalised_density(density_with_params,
                                          max_search_step)

    # Stop if decided during during density plotting function.
    if plot_result == 'stop':
        return

    # Separate out plot_result for easy use.
    fig, axes = plot_result

    # Get the rejection-based sample and time it.
    print('Sampling')
    start_time = time.time()
    sample_x_values, tries = rejsamp(density_with_params,
                                     final_size,
                                     max_search_step,
                                     sampling_max_multiplier)

    time_elapsed = time.time() - start_time
    print('That took {:.1} s.'.format(time_elapsed))
    print(repr(len(sample_x_values)) + ' data_points sampled from '
          + repr(tries) + ' attempts.')

    # Plot histogram, also normalised to sum to 1.
    print('Plotting sample histogram')
    axes.hist(sample_x_values,
              bins=100,
              range=density_with_params.limits,
              density=True)
    axes.set_title('Samples: ' + repr(len(sample_x_values)) + ', '
                   + 'Tries: ' + repr(tries) + ', '
                   + '\nMax_mutliplier for random density values: '
                   + repr(sampling_max_multiplier),
                   wrap=True)
    fig.show()

    return


if __name__ == '__main__':
    main()
