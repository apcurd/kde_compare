# -*- coding: utf-8 -*-
"""
test_kernel_forms.py

Functions for testing kernel_forms.py

Created on Thu Jul  9 16:43:10 2020

Authored by
Alistair Curd
Peckham lab
School of Molecular and Cellular Biology
Faculty of Biological Sciences
University of Leeds

---
Copyright 2020 Alistair Curd

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at
http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the License for the
specific language governing permissions and limitations under the License.
"""


import numpy as np
from numpy import testing as nptest

import kernel_forms as kf


def test_gaussian():
    """Test gaussian()"""
    # Try running with single value input
    expected_3_2 = 1. / (2. * np.sqrt(2. * np.pi)) \
        * np.exp(-1. * 3. ** 2 / (2. * 2. ** 2))
    assert kf.gaussian(3., 2.) == expected_3_2

    # Check integer casting
    assert kf.gaussian(3, 2) == expected_3_2

    # Check result with numpy array
    expected_5_2 = 1. / (2. * np.sqrt(2. * np.pi)) \
        * np.exp(-1. * 5. ** 2 / (2. * 2. ** 2))
    expected_array = np.array([expected_3_2, expected_5_2])
    distance_input_array = np.array([3., 5.])
    nptest.assert_array_equal(kf.gaussian(distance_input_array, 2.),
                              expected_array)
