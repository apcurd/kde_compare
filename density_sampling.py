"""
theoretical_densities.py

Density functions from which to sample data.

Created on Tue Jul 14 16:53:44 2020

Authored by
Alistair Curd
Peckham lab
School of Molecular and Cellular Biology
Faculty of Biological Sciences
University of Leeds

---
Copyright 2020 Alistair Curd

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at
http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the License for the
specific language governing permissions and limitations under the License.

References
----------
Karunammi(2005):
    R.J. Karunammi & T. Alberts, Statistical Methodology 2 (2005) 191-212
"""

import numpy as np
from scipy.integrate import quad as integ


def karunammi_1(data_points):
    """Density 1 in Karunammi(2005).

    Args
    ----
        data_points : int, float or numpy array
            Location(s) at which the density will be generated.

    Returns
    -------
        density at data locations : numpy array
    """
    return data_points ** 2 / 2. * np.exp(-data_points)


def karunammi_2(data_points):
    """Density 2 in Karunammi(2005).

    Args
    ----
        data_points : int, float or numpy array
            Location(s) at which the density will be generated.

    Returns
    -------
        density at data locations : numpy array
    """
    return 2 / (np.pi * (1 + data_points ** 2))


def karunammi_3(data_points):
    """Density 3 in Karunammi(2005).

    Args
    ----
        data_points : int, float or numpy array
            Location(s) at which the density will be generated.

    Returns
    -------
        density at data locations : numpy array
    """
    return 5 / 4 * (1 + 15 * data_points) * np.exp(-5 * data_points)


def karunammi_4(data_points):
    """Density 4 in Karunammi(2005).

    Args
    ----
        data_points : int, float or numpy array
            Location(s) at which the density will be generated.

    Returns
    -------
        density at data locations : numpy array
    """
    return 5 * np.exp(-5 * data_points)


def sine_plus_one(data_points, wavelength):
    """Sinusoidal density variation.

    Args
    ----
    data_points (int, float or numpy array):
        Location(s) at which the density will be generated.
    wavelength (float):
        Wavelength of the variation.

    Returns
    -------
    density (numpy array):
        sin(2 * pi * x / wavelength) + 1, where x = data_points
    """
    return 1 + np.sin(2 * np.pi * data_points / wavelength)


class DensityWithParams:
    """Defines an object to hold a density function and associated info.

    Args
    ----
    distribution (function):
        Function describing the shape of the data density distribution.
        Arguments are data values, and parameters for the function
        shape.
    params (list):
        Arguments to the density function, other than the data values.
    limits (tuple, length 2):
        Minimum and maximum data values to be used with this density
        object.

    Attributes
    ----------
    distribution (function):
        Function describing the shape of the data density distribution.
        Arguments are data values, and parameters for the function
        shape.
    params (list):
        Arguments to the density function, other than the data values.
    limits (tuple, length 2):
        Minimum and maximum data values to be used with this density
        object.
    norm_factor (float):
        Multiplier for normalising the function to integrate. Default
        value is 1.

    Methods
    -------
    get_norm_factor
    """

    def __init__(self, distribution, params, limits):
        """Initialise."""
        self.distribution = distribution
        self.params = params
        self.limits = limits
        self.norm_factor = 1.  # Default value

    def get_norm_factor(self):
        """Calculate a multiplier for normalising the density function."""
        # Integrate the density over the range of the data values
        (function_integral,
         integral_error) = integ(self.distribution,
                                 self.limits[0], self.limits[1],
                                 args=(*self.params,))
        print('Integral of this distribution over this data range is '
              + repr(function_integral)
              + ' +- '
              + repr(integral_error) + '.')
        print('^ CHECK YOU ARE HAPPY. ^')
        self.norm_factor = 1. / function_integral


def rejection_sample(density_with_params,
                     final_size,
                     max_search_step=1.,
                     sampling_max_multiplier=1.05):
    """Rejection sampling from a density function.

    Generate random sample from density function using von Neumann's
    rejection sampling. (And see Bayesian Data Analysis (Gelman, Carlin,
    Stern, Rubin), 2nd Ed., p284-5.)

    Note:
        Normalisation of the density distribution is not required for
        rejection sampling.

    Args:
        density_with_params : DensityWithParams object
            Includes the density distribution and data value limits.
        final_size : int
            Number of data points in the final sample.
        max_search_step : float
            The step size over density function at which to find the
            function maximum.
        sampling_max_multiplier : float
            Multiplier to the maximum of the density_function that
            controls the ratio of accepted/rejected sample attempts.

    Returns:
        sample_x_values : numpy array
            Simulated random data from the input density function
        tries: int
            How many random samples were taken in total to get acquire
            the sample.
    """
    # Find values of density distribution to access a maximum value,
    # using x-steps small enough to capture variation
    x_values = np.arange(density_with_params.limits[0],
                         density_with_params.limits[1],
                         max_search_step)
    density_values = density_with_params.distribution(
        x_values, *density_with_params.params)

    # Set the upper value for the density value random sampling before
    # rejection
    sampling_density_max = sampling_max_multiplier * np.max(density_values)

    # Set up the sample and populate
    sample_x_values = np.array([])
    tries = 0
    while len(sample_x_values) < final_size:
        tries = tries + 1

        # Sample on density axis
        sample_density_attempt = np.random.uniform(0, sampling_density_max)

        # Sample on data values axis
        sample_x_value_attempt = np.random.uniform(
            density_with_params.limits[0], density_with_params.limits[1])

        # Accept sample attempt with probability depending on density
        # at that x_value
        density_at_attempt = density_with_params.distribution(
            sample_x_value_attempt, *density_with_params.params)
        if sample_density_attempt < density_at_attempt:
            sample_x_values = np.append(sample_x_values,
                                        sample_x_value_attempt)

    return sample_x_values, tries
